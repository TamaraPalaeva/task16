package ru.sberbank.path2;

public enum Orientation {

    NORTH, WEST, SOUTH, EAST;

}
